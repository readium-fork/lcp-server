#!/bin/sh
npm config set registry http://registry.npmjs.org/ --global
npm cache clear --force
npm config set strict-ssl false
git config --global url.https://github.com/.insteadOf git://github.com/
rm -rf node-modules
rm package-lock.json
npm install
npm run-script build