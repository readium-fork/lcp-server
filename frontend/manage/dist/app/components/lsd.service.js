"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var LsdService = /** @class */ (function () {
    function LsdService(http) {
        this.http = http;
        this.lsdServer = Config.lsd.url; // from Config
    }
    LsdService_1 = LsdService;
    // private headers = new Headers ({'Content-Type': 'application/json'});
    LsdService.getParamsFor = function (id, name) {
        var params = '';
        if (id !== undefined) {
            params += '?id=' + id;
            if (name !== undefined) {
                params += '&name=' + name;
            }
        }
        else if (name !== undefined) {
            params += '?name=' + name;
        }
        return params;
    };
    LsdService.getParams = function (enddate, id, name) {
        var p = LsdService_1.getParamsFor(id, name);
        if (p === '') {
            return '?end=' + enddate.toISOString();
        }
        else {
            p += '&end=' + enddate.toISOString();
            return p;
        }
    };
    LsdService.prototype.getStatus = function (licenseID, id, name) {
        return this.http.get(this.lsdServer + '/licenses/' + licenseID + '/status' + LsdService_1.getParamsFor(id, name))
            .toPromise()
            .then(function (response) {
            if ((response.status === 200) || (response.status === 201)) {
                return response.json();
            }
            else {
                throw 'Error in getStatus(License Status Document); ' + response.status + response.text;
            }
        })
            .catch(this.handleError);
    };
    LsdService.prototype.registerDevice = function (licenseID, id, name) {
        return this.http.post(this.lsdServer + '/licenses/' + licenseID + '/register' + LsdService_1.getParamsFor(id, name), undefined)
            .toPromise()
            .then(function (response) {
            if ((response.status === 200) || (response.status === 201)) {
                return response.json();
            }
            else if ((response.status === 400)) { // bad request
                var obj = response.json();
                throw 'Error registering device (License Status Document): ' + obj.detail + '\n' + response.status + response.text;
            }
            else if ((response.status === 404)) { // license not found
                var obj = response.json();
                throw 'License not found: ' + obj.detail + '\n' + response.status + response.text;
            }
            else {
                throw 'Error registering device (License Status Document); ' + response.status + response.text;
            }
        })
            .catch(this.handleError);
    };
    LsdService.prototype.returnLoan = function (licenseID, id, name) {
        return this.http.put(this.lsdServer + '/licenses/' + licenseID + '/return' + LsdService_1.getParamsFor(id, name), undefined)
            .toPromise()
            .then(function (response) {
            if ((response.status === 200) || (response.status === 201)) {
                return response.json();
            }
            else {
                throw 'Error in returnLoan(License Status Document); ' + response.status + response.text;
            }
        })
            .catch(this.handleError);
    };
    LsdService.prototype.renewLoan = function (licenseID, endLicense, id, name) {
        return this.http.put(this.lsdServer + '/licenses/' + licenseID + '/renew' + LsdService_1.getParams(endLicense, id, name), undefined)
            .toPromise()
            .then(function (response) {
            if ((response.status === 200) || (response.status === 201)) {
                return response.json();
            }
            else if ((response.status === 400)) {
                var obj = response.json();
                throw 'Error in renewLoan(License Status Document): ' + obj.detail + '\n' + response.status + response.text;
            }
            else {
                throw 'Error in renewLoan(License Status Document); ' + response.status + response.text;
            }
        })
            .catch(this.handleRenewError);
    };
    LsdService.prototype.handleError = function (error) {
        console.error('An error occurred (lsd-service)', error);
        return Promise.reject(error.message || error);
    };
    LsdService.prototype.handleRenewError = function (error) {
        console.error('Error renew (lsd-service)', error);
        return Promise.reject(error);
    };
    var LsdService_1;
    LsdService = LsdService_1 = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], LsdService);
    return LsdService;
}());
exports.LsdService = LsdService;
//# sourceMappingURL=lsd.service.js.map