"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var lic = require("./partialLicense");
/*
 * Return only the rights of a partial license (or undefined)
 * Takes partialLicense as a string argument
 * Usage:
 *   partialLicense | filterRights
 */
var FilterRights = /** @class */ (function () {
    function FilterRights() {
    }
    FilterRights.prototype.transform = function (partialLicense) {
        var r = new lic.UserRights;
        var obj;
        obj = JSON.parse(partialLicense, function (key, value) {
            if (typeof value === 'string') {
                var a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                if (a) {
                    return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]));
                }
            }
            return value;
        });
        if (obj.rights) {
            console.log(obj.rights);
            r = obj.rights;
            return r;
        }
        return undefined;
    };
    FilterRights = __decorate([
        core_1.Pipe({ name: 'FilterRights' })
    ], FilterRights);
    return FilterRights;
}());
exports.FilterRights = FilterRights;
var ShowRights = /** @class */ (function () {
    function ShowRights() {
    }
    ShowRights.prototype.transform = function (partialLicense) {
        var r = new lic.UserRights;
        var obj;
        obj = JSON.parse(partialLicense, function (key, value) {
            if (typeof value === 'string') {
                var a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                if (a) {
                    return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]));
                }
            }
            return value;
        });
        if (obj.rights) {
            console.log(obj.rights);
            r = obj.rights;
            var s = '';
            if (r.copy > 0) {
                s = 'copy=' + r.copy + ', print=' + r.print + ' ';
            }
            return s + 'available from ' + r.start.toLocaleString() + ' to ' + r.end.toLocaleString();
        }
        return '';
    };
    ShowRights = __decorate([
        core_1.Pipe({ name: 'ShowRights' })
    ], ShowRights);
    return ShowRights;
}());
exports.ShowRights = ShowRights;
//# sourceMappingURL=rightsFromPartialLicense.pipe.js.map