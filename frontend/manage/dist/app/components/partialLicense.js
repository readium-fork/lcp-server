"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BASIC_PROFILE = 'http://readium.org/lcp/basic-profile';
exports.V1_PROFILE = 'http://readium.org/lcp/profile-1.0';
exports.USERKEY_ALGO = 'http://www.w3.org/2001/04/xmlenc#sha256';
exports.PROVIDER = 'http://edrlab.org';
var Key = /** @class */ (function () {
    function Key() {
    }
    return Key;
}());
exports.Key = Key;
var ContentKey = /** @class */ (function (_super) {
    __extends(ContentKey, _super);
    function ContentKey() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ContentKey;
}(Key));
exports.ContentKey = ContentKey;
var UserKey = /** @class */ (function (_super) {
    __extends(UserKey, _super);
    function UserKey() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return UserKey;
}(Key));
exports.UserKey = UserKey;
var Encryption = /** @class */ (function () {
    function Encryption() {
    }
    return Encryption;
}());
exports.Encryption = Encryption;
var Link = /** @class */ (function () {
    function Link() {
    }
    return Link;
}());
exports.Link = Link;
;
var UserRights = /** @class */ (function () {
    function UserRights() {
    }
    return UserRights;
}());
exports.UserRights = UserRights;
var UserInfo = /** @class */ (function () {
    function UserInfo() {
    }
    return UserInfo;
}());
exports.UserInfo = UserInfo;
var PartialLicense = /** @class */ (function () {
    function PartialLicense() {
    }
    return PartialLicense;
}());
exports.PartialLicense = PartialLicense;
var PartialLicenseJSON = /** @class */ (function (_super) {
    __extends(PartialLicenseJSON, _super);
    function PartialLicenseJSON() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return PartialLicenseJSON;
}(PartialLicense));
exports.PartialLicenseJSON = PartialLicenseJSON;
//# sourceMappingURL=partialLicense.js.map