"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var publication_edit_component_1 = require("./publication-edit.component");
var publication_add_component_1 = require("./publication-add.component");
var publication_list_component_1 = require("./publication-list.component");
var publicationRoutes = [
    { path: 'publications/:id/edit', component: publication_edit_component_1.PublicationEditComponent },
    { path: 'publications/add', component: publication_add_component_1.PublicationAddComponent },
    { path: 'publications', component: publication_list_component_1.PublicationListComponent }
];
var PublicationRoutingModule = /** @class */ (function () {
    function PublicationRoutingModule() {
    }
    PublicationRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(publicationRoutes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], PublicationRoutingModule);
    return PublicationRoutingModule;
}());
exports.PublicationRoutingModule = PublicationRoutingModule;
//# sourceMappingURL=publication-routing.module.js.map