"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var publication_service_1 = require("./publication.service");
var PublicationListComponent = /** @class */ (function () {
    function PublicationListComponent(publicationService) {
        this.publicationService = publicationService;
        this.search = "";
        this.reverse = false;
        this.publications = [];
        this.order = "id";
        this.reverse = true;
    }
    PublicationListComponent.prototype.refreshPublications = function () {
        var _this = this;
        this.publicationService.list().then(function (publications) {
            _this.publications = publications;
        });
    };
    PublicationListComponent.prototype.orderBy = function (newOrder) {
        if (newOrder == this.order) {
            this.reverse = !this.reverse;
        }
        else {
            this.reverse = false;
            this.order = newOrder;
        }
    };
    PublicationListComponent.prototype.keptWithFilter = function (pub) {
        if (pub.title.toUpperCase().includes(this.search.toUpperCase())) {
            return true;
        }
        return false;
    };
    PublicationListComponent.prototype.ngOnInit = function () {
        this.refreshPublications();
    };
    PublicationListComponent.prototype.onRemove = function (objId) {
        var _this = this;
        this.publicationService.delete(objId).then(function (publication) {
            _this.refreshPublications();
        });
    };
    PublicationListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'lcp-publication-list',
            templateUrl: 'publication-list.component.html',
        }),
        __metadata("design:paramtypes", [publication_service_1.PublicationService])
    ], PublicationListComponent);
    return PublicationListComponent;
}());
exports.PublicationListComponent = PublicationListComponent;
//# sourceMappingURL=publication-list.component.js.map