"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var publication_1 = require("./publication");
var publication_service_1 = require("./publication.service");
var ng2_file_upload_1 = require("ng2-file-upload");
var PublicationFormComponent = /** @class */ (function () {
    function PublicationFormComponent(fb, router, publicationService) {
        this.fb = fb;
        this.router = router;
        this.publicationService = publicationService;
        this.baseUrl = Config.frontend.url;
        this.hideFilename = false;
        this.submitButtonLabel = "Add";
        this.snackMessage = "";
        this.errorMessage = "";
        this.hasBaseDropZoneOver = false;
        this.notAPublication = false;
        // onItemAdded is executed when a file is added to the opload component
        this.onItemAdded = function (fileItem) {
            this.split = fileItem.file.name.split('.');
            var extension = this.split[this.split.length - 1];
            if (extension === "epub" || extension === "pdf" || extension === "lpf" ||
                extension === "webpub" || extension === "audiobook" || extension === "divina") {
                this.notAPublication = false;
            }
            else {
                this.notAPublication = true;
            }
            this.uploader.queue = [fileItem];
            this.droppedItem = fileItem;
        };
    }
    PublicationFormComponent.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    PublicationFormComponent.prototype.refreshMasterFiles = function () {
        var _this = this;
        this.publicationService.getMasterFiles().then(function (masterFiles) {
            _this.masterFiles = masterFiles;
        });
    };
    PublicationFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.uploader = new ng2_file_upload_1.FileUploader({ url: this.baseUrl + "/publicationUpload" });
        this.refreshMasterFiles();
        // Events declarations
        this.uploader.onAfterAddingFile = function (fileItem) { _this.onItemAdded(fileItem); fileItem.withCredentials = false; };
        this.uploader.onCompleteAll = function () { _this.gotoList(); };
        // case = import of a new publication
        if (this.publication == null) {
            this.submitButtonLabel = "Add";
            this.form = this.fb.group({
                "title": ["", forms_1.Validators.required],
                "filename": ["", forms_1.Validators.required],
                "type": ["UPLOAD", forms_1.Validators.nullValidator]
            });
            // case = edition of an existing publication
        }
        else {
            this.hideFilename = true;
            this.submitButtonLabel = "Save";
            this.form = this.fb.group({
                "title": [this.publication.title, forms_1.Validators.required]
            });
        }
    };
    PublicationFormComponent.prototype.gotoList = function () {
        this.router.navigate(['/publications']);
    };
    PublicationFormComponent.prototype.onCancel = function () {
        this.gotoList();
    };
    // onSubmit imports a publication into the frontend server, 
    // or updates information on an existing publication.
    // confirm indicates if the user must provide a confirmation in case a publication
    // already exists with the same title.  
    PublicationFormComponent.prototype.onSubmit = function (confirm) {
        var _this = this;
        // case = edition of an existing publication
        if (this.publication) {
            this.publication.title = this.form.value['title'];
            this.publicationService.update(this.publication).then(function (publication) {
                _this.gotoList();
            });
            // case = import of a new publication
        }
        else {
            // if the import into the frontend server needs confirmation (in case of a detected duplicate) 
            if (confirm) {
                // check the title chosen for the publication
                this.publicationService.checkByName(this.form.value['title']).then(function (result) {
                    // if there is no duplicate
                    if (result === 0) {
                        // upload the publication
                        if (_this.form.value["type"] === "UPLOAD") {
                            var options = { url: _this.baseUrl + "/publicationUpload?title=" + _this.form.value['title'] };
                            _this.uploader.setOptions(options);
                            _this.uploader.uploadItem(_this.droppedItem);
                            // or request the import of a publication into the frontend server
                        }
                        else {
                            var publication = new publication_1.Publication();
                            publication.title = _this.form.value['title'];
                            publication.masterFilename = _this.form.value['filename'];
                            _this.publicationService.addPublication(publication)
                                .then(function (error) {
                                console.log(error);
                                _this.uploadConfirmation = false;
                                if (error === 200) {
                                    _this.gotoList();
                                }
                                else if (error === 400) {
                                    _this.errorMessage = "The file must be a proper EPUB, PDF or LPF file.";
                                    _this.showSnackBar(false);
                                }
                            });
                        }
                    }
                    else {
                        _this.uploadConfirmation = true;
                        _this.showSnackBar(true);
                    }
                });
                // if the import into the frontend server doesn't need confirmation
            }
            else {
                // just treat the case of an update of the file via upload
                if (this.form.value["type"] === "UPLOAD") {
                    var options = { url: this.baseUrl + "/publicationUpload?title=" + this.form.value['title'] };
                    this.uploader.setOptions(options);
                    this.uploader.uploadItem(this.droppedItem);
                }
                // the case where a new master file is selected for an existing title is not treated here
                // I could be useful, still ... 
                this.gotoList();
            }
        }
    };
    PublicationFormComponent.prototype.showSnackBar = function (stay) {
        var snakeClass = "show stay";
        if (!stay)
            snakeClass = "show";
        var x = $("#snackbar");
        x.attr("class", snakeClass);
        if (!stay)
            setTimeout(function () { $("#snackbar").attr("class", ""); }, 3000);
    };
    PublicationFormComponent.prototype.hideSnackBar = function () {
        var x = $("#snackbar");
        x.attr("class", "");
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", publication_1.Publication)
    ], PublicationFormComponent.prototype, "publication", void 0);
    PublicationFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'lcp-publication-form',
            templateUrl: 'publication-form.component.html'
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            router_1.Router,
            publication_service_1.PublicationService])
    ], PublicationFormComponent);
    return PublicationFormComponent;
}());
exports.PublicationFormComponent = PublicationFormComponent;
//# sourceMappingURL=publication-form.component.js.map