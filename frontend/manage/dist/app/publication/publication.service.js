"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var crud_service_1 = require("../crud/crud.service");
var PublicationService = /** @class */ (function (_super) {
    __extends(PublicationService, _super);
    function PublicationService(http) {
        var _this = _super.call(this) || this;
        _this.http = http;
        _this.baseUrl = Config.frontend.url + '/api/v1/publications';
        _this.masterFileListUrl = Config.frontend.url +
            '/api/v1/repositories/master-files';
        return _this;
    }
    PublicationService.prototype.decode = function (jsonObj) {
        return {
            id: jsonObj.id,
            uuid: jsonObj.uuid,
            title: jsonObj.title,
            status: jsonObj.status,
            masterFilename: null
        };
    };
    PublicationService.prototype.encode = function (obj) {
        return {
            id: obj.id,
            title: obj.title,
            masterFilename: obj.masterFilename
        };
    };
    PublicationService.prototype.checkByName = function (name) {
        var self = this;
        return this.http
            .get(this.baseUrl + "/check-by-title?title=" + name, { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            var jsonObj = response.json();
            return jsonObj;
        })
            .catch(this.handleError);
    };
    PublicationService.prototype.getMasterFiles = function () {
        return this.http
            .get(this.masterFileListUrl, { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            if (response.ok) {
                var items = [];
                for (var _i = 0, _a = response.json(); _i < _a.length; _i++) {
                    var jsonObj = _a[_i];
                    items.push({
                        name: jsonObj.name
                    });
                }
                return items;
            }
            else {
                throw 'Error creating user ' + response.text;
            }
        })
            .catch(this.handleError);
    };
    PublicationService.prototype.addPublication = function (pub) {
        return this.http
            .post(this.baseUrl, this.encode(pub), { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            if (response.ok) {
                return 200;
            }
            else {
                throw 'Error creating publication ' + response.text;
            }
        })
            .catch(this.handleAddError);
    };
    PublicationService.prototype.handleAddError = function (error) {
        return error.status;
    };
    PublicationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], PublicationService);
    return PublicationService;
}(crud_service_1.CrudService));
exports.PublicationService = PublicationService;
//# sourceMappingURL=publication.service.js.map