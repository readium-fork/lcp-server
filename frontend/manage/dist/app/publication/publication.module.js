"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var publication_service_1 = require("./publication.service");
var publication_routing_module_1 = require("./publication-routing.module");
var publication_add_component_1 = require("./publication-add.component");
var publication_edit_component_1 = require("./publication-edit.component");
var publication_list_component_1 = require("./publication-list.component");
var publication_form_component_1 = require("./publication-form.component");
var ng2_file_upload_1 = require("ng2-file-upload");
var sort_module_1 = require("../shared/pipes/sort.module");
var PublicationModule = /** @class */ (function () {
    function PublicationModule() {
    }
    PublicationModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                publication_routing_module_1.PublicationRoutingModule,
                ng2_file_upload_1.FileUploadModule,
                sort_module_1.SortModule
            ],
            declarations: [
                publication_add_component_1.PublicationAddComponent,
                publication_edit_component_1.PublicationEditComponent,
                publication_list_component_1.PublicationListComponent,
                publication_form_component_1.PublicationFormComponent
            ],
            providers: [
                publication_service_1.PublicationService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        })
    ], PublicationModule);
    return PublicationModule;
}());
exports.PublicationModule = PublicationModule;
//# sourceMappingURL=publication.module.js.map