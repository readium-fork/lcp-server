"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var CrudService = /** @class */ (function () {
    function CrudService() {
        this.defaultHttpHeaders = new http_1.Headers({ 'Content-Type': 'application/json' });
    }
    CrudService.prototype.list = function () {
        var self = this;
        return this.http.get(this.baseUrl, { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            var items = [];
            for (var _i = 0, _a = response.json(); _i < _a.length; _i++) {
                var jsonObj = _a[_i];
                items.push(self.decode(jsonObj));
            }
            return items;
        })
            .catch(this.handleError);
    };
    CrudService.prototype.get = function (id) {
        var self = this;
        return this.http
            .get(this.baseUrl + "/" + id, { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            var jsonObj = response.json();
            return self.decode(jsonObj);
        })
            .catch(this.handleError);
    };
    CrudService.prototype.delete = function (id) {
        var self = this;
        return this.http.delete(this.baseUrl + "/" + id)
            .toPromise()
            .then(function (response) {
            if (response.ok) {
                return true;
            }
            else {
                throw 'Error creating user ' + response.text;
            }
        })
            .catch(this.handleError);
    };
    CrudService.prototype.add = function (obj) {
        return this.http
            .post(this.baseUrl, this.encode(obj), { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            if (response.ok) {
                return obj;
            }
            else {
                throw 'Error creating user ' + response.text;
            }
        })
            .catch(this.handleError);
    };
    CrudService.prototype.update = function (obj) {
        return this.http
            .put(this.baseUrl + "/" + obj.id, this.encode(obj), { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            if (response.ok) {
                return obj;
            }
            else {
                throw 'Error creating user ' + response.text;
            }
        })
            .catch(this.handleError);
    };
    CrudService.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    return CrudService;
}());
exports.CrudService = CrudService;
//# sourceMappingURL=crud.service.js.map