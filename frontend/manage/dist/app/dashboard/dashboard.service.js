"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var DashboardService = /** @class */ (function () {
    function DashboardService(http) {
        this.defaultHttpHeaders = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.http = http;
        this.baseUrl = Config.frontend.url;
        this.masterFileListUrl = Config.frontend.url + '/api/v1/repositories/master-files';
    }
    DashboardService.prototype.decode = function (jsonObj) {
        return {
            publicationCount: jsonObj.publicationCount,
            userCount: jsonObj.userCount,
            buyCount: jsonObj.buyCount,
            loanCount: jsonObj.loanCount
        };
    };
    DashboardService.prototype.get = function () {
        var self = this;
        return this.http
            .get(this.baseUrl + "/dashboardInfos", { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            var jsonObj = response.json();
            return self.decode(jsonObj);
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.getBestSeller = function () {
        var self = this;
        return this.http
            .get(this.baseUrl + "/dashboardBestSellers", { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            if (response.ok) {
                var items = [];
                for (var _i = 0, _a = response.json(); _i < _a.length; _i++) {
                    var jsonObj = _a[_i];
                    items.push(jsonObj);
                }
                return items;
            }
            else {
                throw 'Error creating user ' + response.text;
            }
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    DashboardService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], DashboardService);
    return DashboardService;
}());
exports.DashboardService = DashboardService;
//# sourceMappingURL=dashboard.service.js.map