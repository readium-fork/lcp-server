"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var user_1 = require("./user");
var user_service_1 = require("./user.service");
var UserFormComponent = /** @class */ (function () {
    function UserFormComponent(fb, router, userService) {
        this.fb = fb;
        this.router = router;
        this.userService = userService;
        this.edit = false;
        this.submitButtonLabel = "Add";
        this.submitted = false;
    }
    UserFormComponent.prototype.ngOnInit = function () {
        if (this.user == null) {
            this.user = new user_1.User();
            this.submitButtonLabel = "Add";
            this.form = this.fb.group({
                "name": ["", forms_1.Validators.required],
                "email": ["", forms_1.Validators.required],
                "password": ["", forms_1.Validators.required],
                "hint": ["", forms_1.Validators.required]
            });
        }
        else {
            this.edit = true;
            this.submitButtonLabel = "Save";
            this.form = this.fb.group({
                "name": [this.user.name, forms_1.Validators.required],
                "email": [this.user.email, forms_1.Validators.required],
                "password": "",
                "hint": [this.user.hint, forms_1.Validators.required]
            });
        }
    };
    UserFormComponent.prototype.gotoList = function () {
        this.router.navigate(['/users']);
    };
    UserFormComponent.prototype.onCancel = function () {
        this.gotoList();
    };
    UserFormComponent.prototype.onSubmit = function () {
        var _this = this;
        this.bindForm();
        if (this.edit) {
            this.userService.update(this.user).then(function (user) {
                _this.gotoList();
            });
        }
        else {
            this.userService.add(this.user).then(function (user) {
                _this.gotoList();
            });
        }
        this.submitted = true;
    };
    // Bind form to user
    UserFormComponent.prototype.bindForm = function () {
        this.user.name = this.form.value['name'];
        this.user.email = this.form.value['email'];
        this.user.hint = this.form.value['hint'];
        var newPassword = this.form.value['password'];
        newPassword = newPassword.trim();
        if (newPassword.length > 0) {
            this.user.clearPassword = newPassword;
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", user_1.User)
    ], UserFormComponent.prototype, "user", void 0);
    UserFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'lcp-user-form',
            templateUrl: 'user-form.component.html'
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            router_1.Router,
            user_service_1.UserService])
    ], UserFormComponent);
    return UserFormComponent;
}());
exports.UserFormComponent = UserFormComponent;
//# sourceMappingURL=user-form.component.js.map