"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var user_service_1 = require("./user.service");
var user_routing_module_1 = require("./user-routing.module");
var user_list_component_1 = require("./user-list.component");
var user_form_component_1 = require("./user-form.component");
var user_add_component_1 = require("./user-add.component");
var user_edit_component_1 = require("./user-edit.component");
var sort_module_1 = require("../shared/pipes/sort.module");
var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                user_routing_module_1.UserRoutingModule,
                sort_module_1.SortModule
            ],
            declarations: [
                user_list_component_1.UserListComponent,
                user_form_component_1.UserFormComponent,
                user_add_component_1.UserAddComponent,
                user_edit_component_1.UserEditComponent
            ],
            providers: [
                user_service_1.UserService
            ]
        })
    ], UserModule);
    return UserModule;
}());
exports.UserModule = UserModule;
//# sourceMappingURL=user.module.js.map