"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("./user.service");
var UserListComponent = /** @class */ (function () {
    function UserListComponent(userService) {
        this.userService = userService;
        this.reverse = false;
        this.users = [];
        this.order = "id";
        this.reverse = true;
    }
    UserListComponent.prototype.refreshUsers = function () {
        var _this = this;
        this.userService.list().then(function (users) {
            _this.users = users;
        });
    };
    UserListComponent.prototype.orderBy = function (newOrder) {
        if (newOrder == this.order) {
            this.reverse = !this.reverse;
        }
        else {
            this.reverse = false;
            this.order = newOrder;
        }
    };
    UserListComponent.prototype.ngOnInit = function () {
        this.refreshUsers();
    };
    UserListComponent.prototype.onRemove = function (objId) {
        var _this = this;
        this.userService.delete(objId).then(function (user) {
            _this.refreshUsers();
        });
    };
    UserListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'lcp-user-list',
            templateUrl: 'user-list.component.html'
        }),
        __metadata("design:paramtypes", [user_service_1.UserService])
    ], UserListComponent);
    return UserListComponent;
}());
exports.UserListComponent = UserListComponent;
//# sourceMappingURL=user-list.component.js.map