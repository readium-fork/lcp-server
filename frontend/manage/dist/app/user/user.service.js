"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var jsSHA = require("jssha");
var crud_service_1 = require("../crud/crud.service");
var UserService = /** @class */ (function (_super) {
    __extends(UserService, _super);
    function UserService(http) {
        var _this = _super.call(this) || this;
        _this.http = http;
        _this.baseUrl = Config.frontend.url + '/api/v1/users';
        return _this;
    }
    UserService.prototype.decode = function (jsonObj) {
        return {
            id: jsonObj.id,
            uuid: jsonObj.uuid,
            name: jsonObj.name,
            email: jsonObj.email,
            password: jsonObj.password,
            clearPassword: null,
            hint: jsonObj.hint
        };
    };
    UserService.prototype.encode = function (obj) {
        var jsonObj = {
            id: obj.id,
            uuid: obj.uuid,
            name: obj.name,
            email: obj.email,
            password: obj.password,
            hint: obj.hint
        };
        if (obj.clearPassword == null) {
            // No password change
            return jsonObj;
        }
        // Hash password
        var jsSHAObject = new jsSHA("SHA-256", "TEXT");
        jsSHAObject.update(obj.clearPassword);
        jsonObj['password'] = jsSHAObject.getHash("HEX");
        return jsonObj;
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], UserService);
    return UserService;
}(crud_service_1.CrudService));
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map