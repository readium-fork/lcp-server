"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var not_found_component_1 = require("./not-found.component");
var app_component_1 = require("./app.component");
var app_routing_module_1 = require("./app-routing.module");
var lsd_module_1 = require("./lsd/lsd.module");
var sidebar_module_1 = require("./shared/sidebar/sidebar.module");
var header_module_1 = require("./shared/header/header.module");
var dashboard_module_1 = require("./dashboard/dashboard.module");
var user_module_1 = require("./user/user.module");
var publication_module_1 = require("./publication/publication.module");
var purchase_module_1 = require("./purchase/purchase.module");
var license_module_1 = require("./license/license.module");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                lsd_module_1.LsdModule,
                header_module_1.HeaderModule,
                sidebar_module_1.SidebarModule,
                dashboard_module_1.DashboardModule,
                user_module_1.UserModule,
                publication_module_1.PublicationModule,
                purchase_module_1.PurchaseModule,
                license_module_1.LicenseModule,
                app_routing_module_1.AppRoutingModule
            ],
            declarations: [
                app_component_1.AppComponent,
                not_found_component_1.PageNotFoundComponent
            ],
            bootstrap: [
                app_component_1.AppComponent
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map