"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var license_service_1 = require("./license.service");
var license_routing_module_1 = require("./license-routing.module");
var license_component_1 = require("./license.component");
var license_info_component_1 = require("./license-info.component");
var sort_module_1 = require("../shared/pipes/sort.module");
var LicenseModule = /** @class */ (function () {
    function LicenseModule() {
    }
    LicenseModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule,
                license_routing_module_1.LicenseRoutingModule,
                sort_module_1.SortModule
            ],
            declarations: [
                license_component_1.LicenseComponent,
                license_info_component_1.LicenseInfoComponent
            ],
            providers: [
                license_service_1.LicenseService
            ]
        })
    ], LicenseModule);
    return LicenseModule;
}());
exports.LicenseModule = LicenseModule;
//# sourceMappingURL=license.module.js.map