"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var license_service_1 = require("./license.service");
var LicenseInfoComponent = /** @class */ (function () {
    function LicenseInfoComponent(licenseService) {
        this.licenseService = licenseService;
        this.filter = 0;
        this.filtred = false;
        this.reverse = false;
    }
    LicenseInfoComponent.prototype.ngOnInit = function () {
        this.baseUrl = Config.frontend.url;
        this.refreshInfos();
        this.order = "publicationTitle";
    };
    LicenseInfoComponent.prototype.onSubmit = function () {
        this.filtred = true;
        this.refreshInfos();
    };
    LicenseInfoComponent.prototype.refreshInfos = function () {
        var _this = this;
        this.licenseService.get(this.filter).then(function (infos) {
            _this.licenses = infos;
        });
    };
    LicenseInfoComponent.prototype.orderBy = function (newOrder) {
        if (newOrder == this.order) {
            this.reverse = !this.reverse;
        }
        else {
            this.reverse = false;
            this.order = newOrder;
        }
    };
    LicenseInfoComponent.prototype.keyPressed = function (key) {
        if (key == 13) {
            this.onSubmit();
        }
    };
    __decorate([
        core_1.Input('filterBox'),
        __metadata("design:type", Object)
    ], LicenseInfoComponent.prototype, "filterBox", void 0);
    LicenseInfoComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'lcp-frontend-license-info',
            templateUrl: './license-info.component.html'
        }),
        __metadata("design:paramtypes", [license_service_1.LicenseService])
    ], LicenseInfoComponent);
    return LicenseInfoComponent;
}());
exports.LicenseInfoComponent = LicenseInfoComponent;
//# sourceMappingURL=license-info.component.js.map