"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var LicenseService = /** @class */ (function () {
    function LicenseService(http) {
        this.defaultHttpHeaders = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.http = http;
        this.baseUrl = Config.frontend.url;
        this.lsdUrl = Config.lsd.url;
        this.masterFileListUrl = Config.frontend.url + '/api/v1/repositories/master-files';
    }
    LicenseService.prototype.decode = function (jsonObj) {
        return {
            id: jsonObj.ID,
            publicationTitle: jsonObj.publication_title,
            userName: jsonObj.user_name,
            type: jsonObj.type,
            devices: jsonObj.device_count,
            status: jsonObj.status,
            purchaseID: jsonObj.purchase_id
        };
    };
    LicenseService.prototype.get = function (devices) {
        var self = this;
        var headers = new http_1.Headers;
        return this.http
            .get(this.baseUrl + "/api/v1/licenses?devices=" + devices, { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            var items = [];
            for (var _i = 0, _a = response.json(); _i < _a.length; _i++) {
                var jsonObj = _a[_i];
                items.push(self.decode(jsonObj));
            }
            return items;
        })
            .catch(this.handleError);
    };
    LicenseService.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    LicenseService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], LicenseService);
    return LicenseService;
}());
exports.LicenseService = LicenseService;
//# sourceMappingURL=license.service.js.map