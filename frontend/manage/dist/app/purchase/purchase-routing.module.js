"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var purchase_add_component_1 = require("./purchase-add.component");
var purchase_edit_component_1 = require("./purchase-edit.component");
var purchase_status_component_1 = require("./purchase-status.component");
var purchase_list_component_1 = require("./purchase-list.component");
var purchaseRoutes = [
    { path: 'purchases/:id/renew', component: purchase_edit_component_1.PurchaseEditComponent },
    { path: 'purchases/:id/status', component: purchase_status_component_1.PurchaseStatusComponent },
    { path: 'purchases/add', component: purchase_add_component_1.PurchaseAddComponent },
    { path: 'purchases', component: purchase_list_component_1.PurchaseListComponent }
];
var PurchaseRoutingModule = /** @class */ (function () {
    function PurchaseRoutingModule() {
    }
    PurchaseRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(purchaseRoutes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], PurchaseRoutingModule);
    return PurchaseRoutingModule;
}());
exports.PurchaseRoutingModule = PurchaseRoutingModule;
//# sourceMappingURL=purchase-routing.module.js.map