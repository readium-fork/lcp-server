"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var ng2_datetime_picker_1 = require("ng2-datetime-picker");
var purchase_service_1 = require("./purchase.service");
var purchase_routing_module_1 = require("./purchase-routing.module");
var purchase_list_component_1 = require("./purchase-list.component");
var purchase_form_component_1 = require("./purchase-form.component");
var purchase_edit_component_1 = require("./purchase-edit.component");
var purchase_status_component_1 = require("./purchase-status.component");
var purchase_add_component_1 = require("./purchase-add.component");
var sort_module_1 = require("../shared/pipes/sort.module");
var PurchaseModule = /** @class */ (function () {
    function PurchaseModule() {
    }
    PurchaseModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                purchase_routing_module_1.PurchaseRoutingModule,
                ng2_datetime_picker_1.Ng2DatetimePickerModule,
                sort_module_1.SortModule
            ],
            declarations: [
                purchase_list_component_1.PurchaseListComponent,
                purchase_form_component_1.PurchaseFormComponent,
                purchase_edit_component_1.PurchaseEditComponent,
                purchase_status_component_1.PurchaseStatusComponent,
                purchase_add_component_1.PurchaseAddComponent
            ],
            providers: [
                purchase_service_1.PurchaseService
            ]
        })
    ], PurchaseModule);
    return PurchaseModule;
}());
exports.PurchaseModule = PurchaseModule;
//# sourceMappingURL=purchase.module.js.map