"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_slugify_1 = require("ng2-slugify");
var moment = require("moment");
var purchase_service_1 = require("./purchase.service");
var PurchaseListComponent = /** @class */ (function () {
    function PurchaseListComponent(purchaseService) {
        this.purchaseService = purchaseService;
        this.search = "";
        this.reverse = false;
        this.slug = new ng2_slugify_1.Slug('default');
        this.purchases = [];
        this.order = "id";
        this.reverse = true;
    }
    PurchaseListComponent.prototype.refreshPurchases = function () {
        var _this = this;
        this.purchaseService.list().then(function (purchases) {
            _this.purchases = purchases;
        });
    };
    PurchaseListComponent.prototype.orderBy = function (newOrder) {
        if (newOrder == this.order) {
            this.reverse = !this.reverse;
        }
        else {
            this.reverse = false;
            this.order = newOrder;
        }
    };
    PurchaseListComponent.prototype.keptWithFilter = function (pur) {
        if (pur.publication.title.toUpperCase().includes(this.search.toUpperCase()) || pur.user.name.toUpperCase().includes(this.search.toUpperCase())) {
            return true;
        }
        return false;
    };
    PurchaseListComponent.prototype.buildLicenseDeliveredClass = function (licenseUuid) {
        if (licenseUuid == null) {
            return "danger";
        }
        return "success";
    };
    PurchaseListComponent.prototype.buildStatusClass = function (status) {
        if (status == "error") {
            return "danger";
        }
        else if (status == "returned") {
            return "warning";
        }
        return "success";
    };
    PurchaseListComponent.prototype.formatDate = function (date) {
        return moment(date).format('YYYY-MM-DD HH:mm');
    };
    PurchaseListComponent.prototype.ngOnInit = function () {
        this.refreshPurchases();
    };
    PurchaseListComponent.prototype.onRemove = function (objId) {
        var _this = this;
        this.purchaseService.delete(objId).then(function (purchase) {
            _this.refreshPurchases();
        });
    };
    PurchaseListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'lcp-purchase-list',
            templateUrl: 'purchase-list.component.html'
        }),
        __metadata("design:paramtypes", [purchase_service_1.PurchaseService])
    ], PurchaseListComponent);
    return PurchaseListComponent;
}());
exports.PurchaseListComponent = PurchaseListComponent;
//# sourceMappingURL=purchase-list.component.js.map