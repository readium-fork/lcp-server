"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var crud_service_1 = require("../crud/crud.service");
var PurchaseService = /** @class */ (function (_super) {
    __extends(PurchaseService, _super);
    function PurchaseService(http) {
        var _this = _super.call(this) || this;
        _this.baseLSDUrl = Config.lsd.url;
        _this.http = http;
        _this.baseUrl = Config.frontend.url + '/api/v1/purchases';
        _this.baseLSDUrl = Config.lsd.url;
        return _this;
    }
    PurchaseService.prototype.decode = function (jsonObj) {
        return {
            id: jsonObj.id,
            uuid: jsonObj.uuid,
            publication: {
                id: jsonObj.publication.id,
                uuid: jsonObj.publication.uuid,
                title: jsonObj.publication.title
            },
            user: {
                id: jsonObj.user.id,
                uuid: jsonObj.uuid,
                name: jsonObj.user.name,
                email: jsonObj.user.email
            },
            type: jsonObj.type,
            licenseUuid: jsonObj.licenseUuid,
            transactionDate: jsonObj.transactionDate,
            startDate: jsonObj.startDate,
            endDate: jsonObj.endDate,
            status: jsonObj.status
        };
    };
    PurchaseService.prototype.encode = function (obj) {
        return {
            id: Number(obj.id),
            uuid: obj.uuid,
            publication: {
                id: Number(obj.publication.id)
            },
            user: {
                id: Number(obj.user.id)
            },
            type: obj.type,
            licenseUuid: obj.licenseUuid,
            startDate: obj.startDate,
            endDate: obj.endDate,
            status: obj.status
        };
    };
    PurchaseService.prototype.getLicense = function (id) {
        var licenseUrl = this.baseUrl + "/" + id + "/license";
        return this.http
            .get(licenseUrl, { headers: this.defaultHttpHeaders })
            .toPromise()
            .then(function (response) {
            if (response.ok) {
                return response.text();
            }
            else {
                throw 'Error retrieving license ' + response.text();
            }
        })
            .catch(this.handleError);
    };
    PurchaseService.prototype.revoke = function (message, licenseID) {
        var headers = new http_1.Headers;
        headers.append('Authorization', 'Basic ' + btoa(Config.lsd.user + ":" + Config.lsd.password));
        return this.http
            .patch(this.baseLSDUrl + "/licenses/" + licenseID + "/status", { status: "revoked",
            message: "Your license has been revoked because overused." }, { headers: headers })
            .toPromise()
            .then(function (response) {
            if (response.ok) {
                return 200;
            }
            return;
        })
            .catch(this.handleRevokeError);
    };
    PurchaseService.prototype.handleRevokeError = function (error) {
        return error.status;
    };
    PurchaseService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], PurchaseService);
    return PurchaseService;
}(crud_service_1.CrudService));
exports.PurchaseService = PurchaseService;
//# sourceMappingURL=purchase.service.js.map