"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Rx_1 = require("rxjs/Rx");
require("rxjs/add/operator/switchMap");
var purchase_service_1 = require("./purchase.service");
var lsd_service_1 = require("../lsd/lsd.service");
var moment = require("moment");
var PurchaseStatusComponent = /** @class */ (function () {
    function PurchaseStatusComponent(route, purchaseService, lsdService) {
        this.route = route;
        this.purchaseService = purchaseService;
        this.lsdService = lsdService;
        this.revokeMessage = "";
    }
    PurchaseStatusComponent.prototype.ngOnInit = function () {
        this.refreshPurchase();
    };
    PurchaseStatusComponent.prototype.refreshPurchase = function () {
        var _this = this;
        this.route.params
            .switchMap(function (params) { return _this.purchaseService.get(params['id']); })
            .subscribe(function (purchase) {
            _this.purchase = purchase;
            if (purchase.licenseUuid) {
                _this.lsdService.get(purchase.licenseUuid).then(function (licenseStatus) {
                    _this.licenseStatus = licenseStatus;
                });
            }
        });
    };
    PurchaseStatusComponent.prototype.formatDate = function (date) {
        return moment(date).format('YYYY-MM-DD HH:mm');
    };
    PurchaseStatusComponent.prototype.onDownload_LSD = function (purchase) {
        // The URL does not resolve to a content-disposition+filename like "ebook_title.lsd"
        // If this were the case, most web browsers would normally just download the linked file.
        // Instead, with some browsers the file is displayed (the current page context is overwritten)
        var url = this.buildLsdDownloadUrl(purchase);
        //document.location.href = url;
        window.open(url, "_blank");
    };
    PurchaseStatusComponent.prototype.buildLsdDownloadUrl = function (purchase) {
        return Config.lsd.url + '/licenses/' + purchase.licenseUuid + '/status';
    };
    PurchaseStatusComponent.prototype.onDownload_LCPL = function (purchase) {
        var _this = this;
        // Wait 5 seconds before refreshing purchases
        var downloadTimer = Rx_1.Observable.timer(5000);
        var downloadSubscriber = downloadTimer.subscribe(function (t) {
            _this.refreshPurchase();
            downloadSubscriber.unsubscribe();
        });
        // The URL resolves to a content-disposition+filename like "ebook_title.lcpl"
        // Most web browsers should normally just download the linked file, not display it.
        var url = this.buildLcplDownloadUrl(purchase);
        window.open(url, "_blank");
    };
    PurchaseStatusComponent.prototype.buildLcplDownloadUrl = function (purchase) {
        return Config.frontend.url + '/api/v1/purchases/' + purchase.id + '/license';
    };
    PurchaseStatusComponent.prototype.onReturn = function (purchase) {
        var _this = this;
        purchase.status = 'to-be-returned';
        this.purchaseService.update(purchase).then(function (purchase) {
            _this.refreshPurchase();
        });
    };
    PurchaseStatusComponent.prototype.onRevoke = function (purchase) {
        var _this = this;
        this.purchaseService.revoke("", purchase.licenseUuid).then(function (status) {
            _this.refreshPurchase();
            var success = false;
            if (status == 200) {
                _this.revokeMessage = "The license has been revoked";
                success = true;
            }
            else if (status == 400) {
                _this.revokeMessage = '400 The new status is not compatible with current status';
            }
            else if (status == 401) {
                _this.revokeMessage = '401 License not Found';
            }
            else if (status == 404) {
                _this.revokeMessage = '404 License not Found';
            }
            else if (status >= 500) {
                _this.revokeMessage = 'An internal error appear';
            }
            else {
                _this.revokeMessage = 'An internal error appear';
            }
            _this.showSnackBar(success);
        });
    };
    PurchaseStatusComponent.prototype.showSnackBar = function (success) {
        var x = $("#snackbar");
        var xClass = "show";
        if (success)
            xClass = "show success";
        x.attr("class", xClass);
        setTimeout(function () { x.attr("class", ""); }, 3000);
    };
    PurchaseStatusComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'lcp-purchase-status',
            templateUrl: 'purchase-status.component.html'
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            purchase_service_1.PurchaseService,
            lsd_service_1.LsdService])
    ], PurchaseStatusComponent);
    return PurchaseStatusComponent;
}());
exports.PurchaseStatusComponent = PurchaseStatusComponent;
//# sourceMappingURL=purchase-status.component.js.map