"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var moment = require("moment");
var purchase_1 = require("./purchase");
var purchase_service_1 = require("./purchase.service");
var user_service_1 = require("../user/user.service");
var user_1 = require("../user/user");
var publication_service_1 = require("../publication/publication.service");
var publication_1 = require("../publication/publication");
var PurchaseFormComponent = /** @class */ (function () {
    function PurchaseFormComponent(fb, router, purchaseService, userService, publicationService) {
        this.fb = fb;
        this.router = router;
        this.purchaseService = purchaseService;
        this.userService = userService;
        this.publicationService = publicationService;
        this.edit = false;
        this.submitButtonLabel = "Add";
        this.submitted = false;
    }
    PurchaseFormComponent.prototype.refreshAvailablePublications = function () {
        var _this = this;
        this.publicationService.list().then(function (publications) {
            _this.availablePublications = publications;
        });
    };
    PurchaseFormComponent.prototype.refreshAvailableUsers = function () {
        var _this = this;
        this.userService.list().then(function (users) {
            _this.availableUsers = users;
        });
    };
    PurchaseFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.refreshAvailablePublications();
        this.refreshAvailableUsers();
        if (this.purchase == null) {
            this.purchase = new purchase_1.Purchase();
            this.submitButtonLabel = "Add";
            this.form = this.fb.group({
                "publication": ["", forms_1.Validators.required],
                "user": ["", forms_1.Validators.required],
                "end_date": ["", forms_1.Validators.required],
                "type": ["LOAN", forms_1.Validators.required]
            });
            this.form.get('type').valueChanges.subscribe(function (value) {
                if (value == "LOAN") {
                    _this.form.get('end_date').setValidators(forms_1.Validators.required);
                }
                else {
                    _this.form.get('end_date').clearValidators();
                }
                _this.form.updateValueAndValidity();
                _this.form.get('end_date').updateValueAndValidity();
            });
        }
        else {
            var dateTime = moment(this.purchase.endDate).format('YYYY-MM-DD HH:mm');
            this.edit = true;
            this.submitButtonLabel = "Save";
            this.form = this.fb.group({
                "renew_type": ["NO_END_DATE", forms_1.Validators.required],
                "end_date": dateTime //[dateTime, Validators.required]
            });
            this.form.get('renew_type').valueChanges.subscribe(function (value) {
                if (value == "NO_END_DATE") {
                    _this.form.get('end_date').clearValidators();
                }
                else {
                    _this.form.get('end_date').setValidators(forms_1.Validators.required);
                }
                _this.form.updateValueAndValidity();
                _this.form.get('end_date').updateValueAndValidity();
            });
        }
    };
    PurchaseFormComponent.prototype.gotoList = function () {
        if (this.edit)
            this.router.navigate(['/purchases/' + this.purchase.id + "/status"]);
        else
            this.router.navigate(['/purchases']);
    };
    PurchaseFormComponent.prototype.onCancel = function () {
        this.gotoList();
    };
    PurchaseFormComponent.prototype.onSubmit = function () {
        var _this = this;
        this.bindForm();
        if (this.edit) {
            this.purchaseService.update(this.purchase).then(function (purchase) {
                _this.gotoList();
            });
        }
        else {
            this.purchaseService.add(this.purchase).then(function (purchase) {
                _this.gotoList();
            });
        }
        this.submitted = true;
    };
    // Bind form to purchase
    PurchaseFormComponent.prototype.bindForm = function () {
        if (!this.edit) {
            var publicationId = this.form.value['publication'];
            var userId = this.form.value['user'];
            var publication = new publication_1.Publication();
            var user = new user_1.User();
            publication.id = publicationId;
            user.id = userId;
            this.purchase.publication = publication;
            this.purchase.user = user;
            this.purchase.type = this.form.value['type'];
        }
        else {
            this.purchase.status = 'to-be-renewed';
        }
        if (this.form.value['end_date'].trim().length > 0) {
            this.purchase.endDate = moment(this.form.value['end_date']).format();
        }
        if (this.edit && this.form.value['renew_type'] == 'NO_END_DATE') {
            // Set end date to null
            // End date will be processed by LSD
            this.purchase.endDate = null;
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", purchase_1.Purchase)
    ], PurchaseFormComponent.prototype, "purchase", void 0);
    PurchaseFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'lcp-purchase-form',
            templateUrl: 'purchase-form.component.html'
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            router_1.Router,
            purchase_service_1.PurchaseService,
            user_service_1.UserService,
            publication_service_1.PublicationService])
    ], PurchaseFormComponent);
    return PurchaseFormComponent;
}());
exports.PurchaseFormComponent = PurchaseFormComponent;
//# sourceMappingURL=purchase-form.component.js.map