"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Sort = /** @class */ (function () {
    function Sort() {
    }
    Sort.prototype.transform = function (value, filter, reverse) {
        var values;
        var getValue = function (a, b, filter) {
            var newA;
            var newB;
            var newFilter = filter.split(".");
            for (var i = 0; i <= newFilter.length - 1; i++) {
                a = a[newFilter[i]];
                b = b[newFilter[i]];
            }
            return {
                a: a,
                b: b
            };
        };
        if (!reverse) {
            value.sort(function (a, b) {
                values = getValue(a, b, filter);
                if (values.a == values.b)
                    return 0;
                else if (values.a > values.b || values.a == null)
                    return 1;
                else if (values.a < values.b || values.b == null)
                    return -1;
            });
        }
        else {
            value.sort(function (a, b) {
                values = getValue(a, b, filter);
                if (values.a == values.b)
                    return 0;
                else if (values.a < values.b || values.a == null)
                    return 1;
                else if (values.a > values.b || values.b == null)
                    return -1;
            });
        }
        return value;
    };
    Sort = __decorate([
        core_1.Pipe({ name: 'sortBy' })
    ], Sort);
    return Sort;
}());
exports.Sort = Sort;
//# sourceMappingURL=sort.pipe.js.map